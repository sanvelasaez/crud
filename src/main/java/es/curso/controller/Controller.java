package es.curso.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import es.curso.producto.Producto;

/**
 * Servlet implementation class Controller
 */
@WebServlet("/Controller")
public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
	ArrayList<Producto> productos;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Controller() {
		if (productos == null) {
			productos = new ArrayList<>();
			productos.add(new Producto("Alicates", "Ferreteria", 5, 25));
			productos.add(new Producto("Destornillador", "Ferreteria", 3, 15));
			productos.add(new Producto("Divan", "Muebleria", 2, 250));
			productos.add(new Producto("Canape", "Muebleria", 3, 135));
			productos.add(new Producto("Manguera", "Jardineria", 3, 5));
			productos.add(new Producto("Grifo", "Jardineria", 10, 25));
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String parametro = (String) request.getParameter("ruta");
		HttpSession miSesion = request.getSession(true);
		miSesion.setAttribute("productos", productos);

		response.sendRedirect("html/" + parametro + ".html");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}
