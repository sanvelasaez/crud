package es.curso.producto;

import java.io.Serializable;

public class Producto implements Serializable{

	private String nombre, seccion;
	private int stock;
	private double precio;
	/**
	 * @param nombre
	 * @param seccion
	 * @param stock
	 * @param precio
	 */
	public Producto(String nombre, String seccion, int stock, double precio) {
		super();
		this.nombre = nombre;
		this.seccion = seccion;
		this.stock = stock;
		this.precio = precio;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the seccion
	 */
	public String getSeccion() {
		return seccion;
	}
	/**
	 * @param seccion the seccion to set
	 */
	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}
	/**
	 * @return the stock
	 */
	public int getStock() {
		return stock;
	}
	/**
	 * @param stock the stock to set
	 */
	public void setStock(int stock) {
		this.stock = stock;
	}
	/**
	 * @return the precio
	 */
	public double getPrecio() {
		return precio;
	}
	/**
	 * @param precio the precio to set
	 */
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	@Override
	public String toString() {
		return "Producto [nombre=" + nombre + ", seccion=" + seccion + ", stock=" + stock + ", precio=" + precio + "]";
	}
	
	
	
}
