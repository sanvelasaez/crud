<%@page import="es.curso.producto.Producto"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:include page="header.jsp">
	<jsp:param value="titulo" name="Modificar producto"/>
</jsp:include>
	<%
	ArrayList<Producto> productos = (ArrayList<Producto>) session.getAttribute("productos");
	boolean existe = false;
	String texto = "";

	String seccion = request.getParameter("seccion"), stock = request.getParameter("stock"),
			precio = request.getParameter("precio");

	for (Producto producto : productos) {
		if (producto.getNombre().equals(request.getParameter("nombre"))) {
			existe = true;
			%>
			<p>
				El producto:
				<%=producto.toString()%></p>
		
			<%
			if (!seccion.equals(""))
				producto.setSeccion(seccion);
			if (!stock.equals(""))
				producto.setStock(Integer.parseInt(stock));
			if (!precio.equals(""))
				producto.setPrecio(Double.parseDouble(precio));
			%>
			<p>
				Ha sido modificado:
				<%=producto.toString()%></p>
			<%
			session.setAttribute("producto", productos);
			break;
			}
		}
	%>
	<p><%=existe ? texto + "Producto modificado" : texto + "El producto no existe"%></p>
<jsp:include page="footer.jsp" />