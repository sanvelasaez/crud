<%@page import="es.curso.producto.Producto"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="header.jsp">
	<jsp:param value="titulo" name="Alta producto"/>
</jsp:include>
	<% ArrayList<Producto> productos = (ArrayList<Producto>) session.getAttribute("productos"); 
	
	String nombre = request.getParameter("nombre"), seccion = request.getParameter("seccion");
	Double precio = Double.valueOf(request.getParameter("precio"));
	int stock = Integer.parseInt(request.getParameter("stock"));
	productos.add(new Producto(nombre,seccion,stock,precio));
	%>
	
	<h1>El producto <%=nombre %>,con precio <%=precio %> ha sido insertado en la
	 sección <%=seccion %> con un stock de <%=stock %></h1>
	
<jsp:include page="footer.jsp" />