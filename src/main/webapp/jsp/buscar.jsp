<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>
<%@page import="es.curso.producto.Producto"%>
<jsp:include page="header.jsp">
	<jsp:param value="titulo" name="buscador" />
</jsp:include>
<%
ArrayList<Producto> productos = (ArrayList<Producto>) session.getAttribute("productos");
String seccion = request.getParameter("seccion").toUpperCase();
%><center>
	<h1>Productos</h1>
	<table border=1>
		<tr>
			<th>Nombre</th>
			<th>Seccion</th>
			<th>Precio</th>
			<th>Stock</th>
		</tr>
		<%
		for (Producto pro : productos) {
			if (pro.getSeccion().toUpperCase().equals(seccion) || seccion.equals("")) {
		%>
		<tr>
			<td><%=pro.getNombre()%></td>
			<td><%=pro.getSeccion()%></td>
			<td><%=pro.getPrecio()%></td>
			<td><%=pro.getStock()%></td>
		</tr>
		<%
		}
		}
		%>
	</table>
</center>
<jsp:include page="footer.jsp" />